# La apasionante guía sobre Soberanía Tecnológica. 

## Índice
### [1. Motivación](es/1_Motivacion.md)
### [2. ¿Qué vamos a abarcar?](es/2_Que_vamos_a_abarcar.md)
### [3. Repaso de conocimientos previos técnicos](es/3_Repaso_de_conocimientos_previos.md)
### [4. Criterios que pueden regir nuestra elección: (hagamos un sondeo!!)](es/4_Criterios.md)
### [5. Vamos a comprar el próximo ordenador para nuestro colectivo.](es/5_Sobre_ordenadores.md)
#### &nbsp;[5.1. Aplicamos nuestros criterios.](es/5_1_Criterios_ordenadores.md)
#### &nbsp;[5.2. ¡Caso práctico!](es/5_2_Caso_practico.md)
#### &nbsp;[5.3. Compendio de referencias de proyectos.](es/5_3_Referencias.md)
### [6. ¡Vamos a comprar la próxima remesa de teléfonos para nuestro colectivo!](es/6_Telefonía.md)
#### &nbsp;[6.1. Aplicamos nuestros criterios (en telefonía).](es/6_1_Criterios_telefonía.md)
#### &nbsp;[6.2. ¡Caso práctico! (en telefonía)](es/6_2_Caso_practico.md)
#### &nbsp;[6.3. Compendio de referencias de proyectos (en telefonía).](es/6_3_Referencias.md)
### [7. Vamos a utilizar una aplicación informática.](es/7_Software.md)
#### &nbsp;[7.1. Aplicamos nuestros criterios (en software).](es/7_1_Criterios_software.md)
#### &nbsp;[7.2. Compendio de referencias de proyectos (en software).](es/7_2_Referencias.md)
### [8. ¿Tenemos una fábrica?](es/8_Industria.md)

<img src="800px-Personal_computer,_exploded_5,_unlabeled.svg.png" alt="Computer"
    title="Computer by parts" width="300"/>

## ¿Qué es esto?

## Participa en la guía

## Licencia del material gráfico de la guía

+ <b>800px-Personal_computer,_exploded_5,_unlabeled.svg.png</b>
    - from Wikimedia user Gustavb
    - licensed under CC-BY 2.5
    - source: https://commons.wikimedia.org/wiki/File:Personal_computer,_exploded_5.svg#/media/File:Personal_computer,_exploded_5,_unlabeled.svg

## Copyright y licencia de la guía

```
Copyright 2021-2024 Mercao Social de Córdoba, La Tejedora. <info(at)latejedora(dot)org>
Pixelada S. Coop. And., 2018-2020
```
© por Mercao Social de Córdoba, La Tejedora., La apasionante guía sobre Soberanía Tecnológica está
disponible bajo una licencia de Creative Commons Reconocimiento 4.0 Internacional.

https://creativecommons.org/licenses/by/4.0/

Por favor, expandid el texto, mejoradlo, corregidlo, compartidlo, no dudéis en escribirnos a:
info (arroba) pixelada (punto) org


#### Revisiones:
- 2024: nueva versión reestructurada en gitlab.
- 2021: empezamos a trabajar en una nueva versión por parte del MErcao Social de córdoba.
- 2019/abril: reestructurado, añadidas referencias de Daniel Melendro (proyecto
    Lamedina), corrección de erratas, criterios distintos para software...
- 2018: trabajo preeliminar.

---
