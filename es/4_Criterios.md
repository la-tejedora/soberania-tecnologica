[< 3. Repaso de conocimientos previos técnicos](3_Repaso_de_conocimientos_previos.md)|[^ Índice](../README.md)|[> 5. Vamos a comprar el próximo ordenador para nuestro colectivo.](5_Sobre_ordenadores.md)
# 4. Criterios que pueden regir nuestra elección: (hagamos un sondeo!!)

+ Buena calidad (que cumpla las expectativas para mi trabajo a realizar).
+ Bajo consumo eléctrico.
+ Diseño abierto y replicable (hardware libre!).
+ Soporte para software libre.
+ Reutilización, reparabilidad y reciclaje.
+ Trazabilidad de los materiales.
+ De proximidad.
+ Condiciones laborales, horizontalidad y transparencia.
+ privacidad y seguridad.
+ Amigabilidad y facilidad de uso.