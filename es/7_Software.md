[< 6.3. Compendio de referencias (en telefonía)](./6_3_Referencias.md)|[^ Índice](../README.md)|[> 7.1. Aplicamos nuestros criterios (En software)](./7_1_Criterios_software.md)

## 7 Vamos a utilizar una aplicación informática.

Se nos debería dar el caso de cuestionarnos porqué instalar una aplicación informática u otra en nuestro teléfonos y ordenadores.

No sólo cual instalar si no además como.

Estas dudas pueden ser incluso mayores cuando son aplicaciones para gestionar tareas sensibles como la contabilidad de una empresa o colectivo.

A la hora de aplicar los criterios anteriormente definidos para equipos físicos, como
teléfonos y ordenadores, nos encontramos con la dificultad de que los programas informáticos no son entes físicos, si no conocimiento en forma de código que ejecutar.

Así, adaptaremos los criterios anteriores de forma que se puedan aplicar a un conocimiento generado y mantenido por una comunidad de personas desarrolladoras y usuarias alrededor del globo.

Habrá que tener en cuenta posteriormente para las aplicaciones cliente – servidor, qué implementación vamos a usar, que colectivo va a coger ese conocimiento ya generado en forma de conocimiento que es el programa informático y lo va a poner en un servidor disponible. (va a implementarlo)

+ Software libre
+ Buena calidad (que cumpla las expectativas para mi trabajo a realizar)
+ Bajo consumo de recursos (memoria y máquina necesaria para ejecutarla)
+ Que esté bien mantenida y con una comunidad fuerte
+ Que sea segura en relación a mi privacidad
+ Buena política comunitaria y de desarrollo
+ En caso de necesitar un servidor, ¿quién y como lo implementa?
+ Amigabilidad y facilidad de uso