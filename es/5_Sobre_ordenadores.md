[< 4. Criterios que pueden regir nuestra elección: (hagamos un sondeo!!)](4_Criterios.md)|[^ Índice](../README.md)|[> 5.1. Criterios aplicados a los ordenadores](5_1_Criterios_ordenadores.md)

# 5. Vamos a comprar el próximo ordenador para nuestro colectivo.

En este capítulo vamos a plantear la tesitura de tener que comprar equipos informáticos para nuestra empresa de Economía Transformadora, colectivo social, o para el uso personal.

La idea es una vez definidos unos criterios éticos de elección, vamos a ver como intentar aplicarlos, en mayor o menor medida, a cada caso concreto de necesidad.
