[< 7.1. Aplicamos nuestros criterios (En software)](./7_1_Criterios_software.md)|[^ Índice](../README.md)|[> 8 ¿Tenemos una fábrica?](./8_Industria.md)

### 7.2.Compendio de referencias de proyectos.

#### Proyectos para pedir ayuda y buscar información:

La wikipedia es maravillosa para artículos técnicos

...

preguntar por programas, alternativas y ayuda:

ask ubuntu:

https://askubuntu.com/

debian forum:

[http://forums.debian.net/](http://forums.debian.net/)

trisquel forum:

https://trisquel.info/en/forum

...

#### Comunidades y proyectos comunitarios:

FSF (https://www.gnu.org/philosophy/license-list.es.html) → página en castellano donde
se referencia distintas licencias de software y su compatibilidad con la licencia libre por
excelencia GPL.

Libreplanet (https://libreplanet.org/wiki/Main_Page) → red globla de entusiastas del
software libre.

#### Iniciativas locales de Economías Transformadoras:

Commonscloud (https://www.commonscloud.coop/) → implementación de una oficina
virtual y nube, para edición de documentos de forma colaborativa.

Katuma (http://katuma.org/) implementación de una plataforma de gestión de grupos →
de consumo.

Comunes (https://comunes.org/) Comunes es un colectivo sin ánimo de lucro dedicado→
a facilitar el trabajo de otros colectivos y activistas mediante el desarrollo de herramientas
web y recursos libres, con el objetivo de fomentar los Bienes Comunes.

Nodo50 (https://info.nodo50.org) Asociación que ofrece alojamiento y servicios web →
desde los años 90. Posee su propia infraestructura de servidores.

Pangea (https://pangea.org) Asociación que ofrece alojamiento y servicios web desde →
los años 90. Posee su propia infraestructura de servidores.

SinDominio (https://sindominio.net) → igual que los anteriores, pero de un perfil más
autogestionado y participativo.

Coopdevs (https://coopdevs.org/es/)

Enreda Cooperativa (https://enreda.coop/) 