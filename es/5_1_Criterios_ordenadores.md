[< 5. Vamos a comprar el próximo ordenador para nuestro colectivo.](5_Sobre_ordenadores.md)|[^ Índice](../README.md)|[> 5.2. ¡Caso Práctico!)](./5_2_Caso_practico.md)

## 5.1.Aplicamos nuestros criterios

### a) Que sea de buena calidad. [^](#51aplicamos-nuestros-criterios)

**Definamos de buena calidad**

La industria tecnológica nos empuja en bloque a consumir nuevos productos _más
potentes_ dejando los anteriores obsoletos.

Dentro de la buena calidad no sólo debería estar incluida la potencia, si no también la
durabilidad, el consumo, la cantidad de energía necesaria para su fabricación...

Vamos a partir de unas necesidades de oficina _estándard_ dentro de un colectivo genérico,
y a establecer en este criterio solamente unos parámetros genéricos en cuanto a
prestaciones técnicas.

La clave de dimensionarlo bien, para trabajos de oficina no hace falta muchas
prestaciones.

+ prestaciones = + consumo y/o + precio. ¿Qué necesitamos realmente?

**Procesador:**

wikipedia es una buena fuente de información, las tiendas no suelen dar detalles
importantes como la memoria caché o el consumo. Queremos:

varios núcleos (procesamiento paralelo = más velocidad) , 64 bits (mejor rendimiento y es
ya lo más estándar), mirar memorias como la caché que suelen ser olvidadas y son muy
importantes, velocidad en Ghz, consumo, ¿llevan GPU incorporada? Mejora el rendimiento
y reduce el consumo.

Es difícil comparar diferentes arquitecturas de procesador :-/ , es decir, un ARM y un x86 a
la misma velocidad no tienen porqué rendir igual.

**Ram:**

mayor cantidad y mayor velocidad, a partir de 2Gb está bien para tareas cotidianas. (quizá
mejor 4Gb)

No pueden combinarse RAMS a distintas velocidades, si se reciclan componentes habría
que ver bien que tipo de RAM soporta nuestro equipo. (buscando en internet con el
modelo del portátil o de la placa base) y cotejando con los módulos que tengamos a
mano.

**Disco Duro:**

los discos duros más modernos, llamados de estado sólido (electrónicos totalmente) son
más rápidos, más resistentes y consumen menos electricidad.


**Gráfica:**

Evitar una tarjeta gráfica extra si ya el microprocesador central o la placa base la lleva
incorporada, incrementará mucho el consumo eléctrico.

Son difíciles de comparar con las prestaciones que nos dan las tiendas convencionales,
en teoría más velocidad y memoria, pero no tiene porqué ser así.

### b) Bajo consumo eléctrico.[^](#51aplicamos-nuestros-criterios)

Como ya hemos dicho no suelen venir las especificaciones de las tiendas, debemos exigir
saber qué consumen nuestros cacharros!

**Los truquis en el consumo de los ordenadores**

¿Nos hace falta una tarjeta gráfica de 200W último modelo?

¿Qué procesador hemos escogido?

Microprocesadores con gráfica incorporada (suele ser ya lo común), wikipedia es un buen
recurso para ver el consumo de los procesadores. (eso no quita que aún no sepamos el
consumo de la placa base)

¡Hay componentes clave en el consumo eléctrico! El tamaño de las pantallas en los
portátiles es determinante, también los discos duros.

No es mala idea tener portátiles de 11’’ (pulgadas) y en casa u oficina trabajar
conectándolos a una pantalla supletoria.

¿Hemos valorado la opción de miniPcs, SBCs? Son como ordenadores de sobremesa (el
mismo concepto pues necesitan de teclado, ratón y pantalla) pero mucho más pequeños,
tienen menos componentes y consumen menos energía.
(SBC) https://es.wikipedia.org/wiki/Placa_computadora

(minipcs en la wikipedia habla de ordenadores en un usb, no me quiero referir a eso, si no
a ordenadores en cajitas, de un tamaño inferior al ATX, que son los ordenadores de
sobremesa de toda la vida, podrían ser mini-itx o micro-atx según la wikipedia, aunque en
las tiendas seguramente los encontréis como minipcs)

https://es.wikipedia.org/wiki/Mini-ITX

https://es.wikipedia.org/wiki/MicroATX

Así los SBC, dispositivos que pueden consumir menos de 20 W, los portátiles y minipcs
consumen más, del orden de 50-100W, y los de sobremesa incluso más de 400 W

Un ejercicio fácil sería multiplicar voltaje (Voltios) y corriente (Amperios) en la salida de
corriente continua del transformador. Así tendríamos los Watios máximos que podría
consumir el equipo.

A la hora de valorar si un equipo merece la pena por su bajo consumo, existe cierto
consenso en valorar previamente la posibilidad de reutilización de equipos antiguos, como
veremos en el siguiente criterio, pues la huella ecológica será seguramente menor.

### c) Posibilidades de reutilización, reparación y reciclaje:[^](#51aplicamos-nuestros-criterios)

¿Podemos reutilizar algún equipo viejo?

Eligiendo bien qué programas le ponemos, que consuman muy poquito, lo podemos
reutilizar!

- Hay **distribuciones de GNU/Linux ligeras.** Suelen basarse en Núcleos Linux más
    antiguos, ya que a versiones más nuevas más prestaciones y más consumo de
    recursos.
    https://en.wikipedia.org/wiki/Light-weight_Linux_distribution
- Jugar con **los programas instalados** , por ejemplo cambiarle el gestor de redes a
    ubuntu y/o debian, usan de gestor de redes network-manager, instalando wicd
    tenemos la misma funcionalidad con menos de la mitad de recursos.
    Ejemplo de experiencia con un pentium III usado para programar, es Debian
    GNU/Linux, con trucos como instalando otro gestor de redes, Awesome como
    escritorio, midori como navegador de internet...
    Se puede usar perfectamente con sólo 256Mb de RAM.
    un **escritorio más ligero** (mate, xfce...) Mismas prestaciones, menos añadidos.
    **ofimática más ligera** (abiword, gnumeric)
    **internet más ligero** (midori, dillo... Claws...)
    https://en.wikipedia.org/wiki/Comparison_of_lightweight_web_browsers
    En definitiva buscar alternativas ligeras a programas cotidianos, hay bastante
    documentación al respecto.
- Los ordenadores de sobremesa son los más reutilizables, pues se pueden
combinar sus piezas.
- Los portátiles se pueden reutilizar cuando se les ha roto algún componente pero
recuperando partes, y no siempre. Si se rompe un componente de la placa base
(procesador, gráfica) se suele romper entero ya que muchas veces van soldados, y
a veces no permite interoperabilidad. (la BIOS está programda para que nuestro
equipo sólo pueda usar determinados componentes y no otros)
Hay trucos como reutilizar la pantalla como pantalla de sobremesa.


```
https://www.instructables.com/id/How-to-Convert-a-Laptop-LCD-into-an-External-
Monit/
```
- Los SBC están muy bien, pues compramos las partes y se ensamblan. Siempre se
    puede cambiar una de ellas.
- Podríamos utilizar **otros materiales**? Ordenadores con carcasas de madera? Están
    trabajando en ello (eoma68) (sería más caro, evitaríamos al menos usar plástico)
    https://www.crowdsupply.com/eoma68/micro-desktop
- Podríamos reutilizar plástico y pedir carcasas fabricadas con plástico reciclado.
    Eso va a requerir otra vez hablar de hardware libre para poder imprimir en 3D
    carcasas de ordenador. Algunas referencias:
    https://en.wikipedia.org/wiki/VIA_OpenBook
    [http://www.print3dworld.es/2013/10/filamaker-convierte-plastico-reciclado-en-](http://www.print3dworld.es/2013/10/filamaker-convierte-plastico-reciclado-en-)
    nuevo-filamento-para-tu-impresora-3d.html
    [http://www.3dimpact.org](http://www.3dimpact.org)
    [http://www.bancodeproyectos.andaluciaemprende.es/es/caso/3d-impact](http://www.bancodeproyectos.andaluciaemprende.es/es/caso/3d-impact)
- Hay proyectos de recuperación de las baterías de litio para una batería de placas
    solares. (ni que decir tiene que es bastante peligroso pues las pilas de litio son
    altamente inflamables, pero bueno, ahí está)
    https://electrek.co/2018/04/17/diy-li-ion-battery/
    https://motherboard.vice.com/en_us/article/kzz7zm/diy-powerwall-builders-are-
    using-recycled-laptop-batteries-to-power-their-homes

En cuanto a reciclaje, no va a haber componentes más reciclables que otros, los circuitos
necesitan de procesos complejos. Quizá la única solución sea reutilizar al máximo y evitar
comprar más.

Valorar también que sean dispositivos sencillos, con pocos componentes, y lo más
modulares posibles.

### d) De proximidad. [^](#51aplicamos-nuestros-criterios)

Es muy difícil o imposible encontrar algo fabricado en Europa, hay empresas que dicen
fabricar aquí, pero suelen referirse al ensamblado.

El ensamblado es la última etapa que al menos sí podemos exigir.
Ejemplo de ordenador portátil de montar una misma:


https://www.olimex.com/Products/DIY-Laptop/KITS/TERES-A64-WHITE/open-source-
hardware

Hay una empresa llamada mountainpc que dice que fabrica aquí, psssse, non me sei.

[http://www.mountain.es](http://www.mountain.es)

Al igual que el reacondicionamiento de dispositivos. Es cuando se cogen equipos del
mercado usados o nuevos, y se modifica el hardware y el software para revenderlos,
ejemplo de technoethical, proyecto europeo con productos certificados por la FSF (Free
Software Foundation):

https://tehnoetic.com/

Os animo a exigir, buscar y compartir la información, que hay muy pocas opciones.

### e) Soportable para software libre, drivers.[^](#51aplicamos-nuestros-criterios)

GNU/linux vs GNU/Linux-libre.

Usar GNU/Linux está bien (Ubuntu, Debian...) usar GNU/Linux-libre está mejor! (Trisquel,
Gnewsense, Parabola...)

https://es.wikipedia.org/wiki/Linux-libre

las tiendas no suelen dar la información importante y necesaria para saber si el equipo
funciona con software libre.

Los componentes críticos suelen ser: tarjeta gráfica, chipset wifi, bluetooth... Impresoras,
escáneres...

la FSF ha dado prioridad a varios proyectos de tarjetas gráficas de CPUs de arquitectura
ARM como la de los teléfonos móviles (ARM) como MALI, vivante, Adreno... (la wiki es
buena fuente para eso)

https://en.wikipedia.org/wiki/Free_and_open-
source_graphics_device_driver#Arm_Holdings

Los sistemas gráficos Intel suelen ser lo más soportados, al igual que los chipset wifi
Atheros. (habría que ver cada modelo en concreto)

Busca información en internet, para ello busca la referencia completa del portátil por
ejemplo: HP pavillion 15 no sirve de nada, hay que buscar que sea el GT123ABC por
ejemplo, que viene detrás de la carcasa. Con esa referencia podemos buscar en internet,
en foros, documentación, h-node, o en la misma web del fabricante para ver las
especificaciones al detalle.


Otra opción es comprarlo ya con GNU/Linux, y a poder ser con GNU/Linux-libre (el gran
problema suele ser ahí las tarjetas gráficas y el wifi, pero si permite pantalla dual y una
resolución aceptable vale)(están mejorando mucho el driver genérico de tarjetas gráficas)

Una prueba suele ser probar con un usb-live una distribución GNU/Linux y ver como se
comporta el sistema. Tenemos wifi? La gráfica permite cambiar bien de resolución? El
sistema va fluído? (hay que poder acceder al ordenador, claro)

https://es.wikipedia.org/wiki/Live_USB

Con el wifi resulta básico entrar en un portal como https://wireless.wiki.kernel.org/ que es
el oficial del sistema Linux para documentar su soporte a las distintas tarjetas wifi.

Normalmente, estas usan un driver libre dentro de Linux, pero este se apoya en un
fragmento de código extra que habrá que instalar, llamado firmware, para poder funcionar
correctamente. Este firmware suele ser de software cerrado, e invalidaría la seguridad de
poder tener un sistema totalmente libre.

En este portal podremos buscar por dispositivos y chip wifis, y nos dirá si necesita de
firmware externo para funcionar o no. Llegado el caso hasta podríamos descargarlo y en
el ver el acuerdo de licencia, si este posee cláusulas abusivas y de no modificación y
republicación del código, es que no es libre.

### f) Diseño abierto y replicable (hardware libre!)[^](#51aplicamos-nuestros-criterios)

Sería ideal si la comunidad de software libre no fuera a remolque de la industria de la
electrónica y la informática.

Hay proyectos de equipos informáticos con diseños libres y replicables, esto se está
dando especialmente en los SBCs.

https://www.olimex.com/Products/DIY-Laptop/KITS/TERES-A64-WHITE/open-source-
hardware

https://en.wikipedia.org/wiki/List_of_open-source_hardware_projects

El problema que acarrea es que es un mundo demasiado industrializado y muy poco
democrático.

Hay proyectos de placas soldables a mano, pero muchas menos prestaciones. Imposible
(prácticamente imposible) trabajar con ellas. (ejemplos de olinuxino y otros)

[http://hforsten.com/making-embedded-linux-computer.html](http://hforsten.com/making-embedded-linux-computer.html)

https://www.olimex.com/Products/OLinuXino/iMX233/iMX233-OLinuXino-MINI/open-
source-hardware


Componentes de inserción olvidados por el mercado. Pero antes fueron también
ordenadores funcionales y reparables. Eran más fáciles de soldar y construir, con pocas
herramientas y preparación.

https://en.wikipedia.org/wiki/Macintosh_128K

https://en.wikipedia.org/wiki/ZX_Spectrum

Esto es importante, porque por muy ético que un proyecto sea en el mundo de la
electrónica, debemos exigirle que su experiencia pueda ser replicable, y sea transparente.
Si no el conocimiento quedará en el olvido.

Reflexión: esta debería ser una condición primordial, a partir de ella podemos pedir
proyectos con mayor trazabilidad, condiciones laborales, proyectos de proximidad, etc. Ya
que el conocimiento sería compartido y podríamos incidir en nuevas necesidades llegando
más lejos en nuestras demandas. (de hecho seguramente saldrían de forma natural)

### g) privacidad. [^](#51aplicamos-nuestros-criterios)

Pinta mal hoy día

**Por hardware:**

Tenemos agujeros en las BIOS, tecnologías con trampas, ejemplo de Intel Management
Engine.

https://libreboot.org/faq.html#intelme

Firmware en dispositivos y microcódigo. (Linux no es libre!)

ARM y microprocesadores cada vez más invasivos e inteligentes. Ejemplo de SOCs con
wifi incorporado y memoria.

https://blog.replicant.us/2013/11/fairphone/

La única opción es usar hardware libre, a efectos de transparencia, con componentes no
invasivos.(ejemplo de los SOCs “que hacen de todo”)

**Por Software:**

El código cerrado es una amenaza para la seguridad. No usar código cerrado.

Usar software libre, pero conociendo bien las fuentes, cuando instalamos una aplicación
instalamos su binario, es decir, alguien ha preparado el código de la aplicación para que ya
pueda ser ejecutado directamente por tu equipo. Tenemos que asegurarnos de que ese
alguien es de confianza :-)

por ejemplo:

Correcto: usar los repositorios libres y oficiales de Debian para instalar una aplicación.


Peligroso: usar un portal X de internet para instalar una aplicación. Ó, insertar el
repositorio de alguien que no conocemos de nada y no es oficial de ningún proyecto
dentro de nuestros repositorios del sistema.

uso de proxies, GNU/Linux y GNU/Linux libre, navegadores seguros (Tor?), evitar la
exposición con redes sociales...

Correos encriptados, encriptado de discos duros...

Buena educación en seguridad (uso correcto de la contraseñas, intentar evitar javascript
con plugins para firefox...)

https://www.fsf.org/campaigns/freejs

Hay mucha formación disponible y colectivos que lo trabajan en profundidad el tema de la
seguridad, como por ejemplo Nodo50.

https://info.nodo50.org/

#### h) Economía Social, condiciones laborales justas, horizontalidad?[^](#51aplicamos-nuestros-criterios)

¿Utópico?

No hay una reapropiación popular de la industria electrónica, cada día que pasa se aleja
más del conocimiento y control popular.

En los 80 y parte de los 90 se fabricaba en Europa y en E.E.U.U. ¿Así que porqué no?

En los 80 proliferaban los Kits de háztelo tu mismo y mucha gente se fabricaba sus
propios ordenadores. Podría estar volviendo.

https://www.olimex.com/Products/DIY-Laptop/

https://hackaday.com/2014/07/11/an-amazing-diy-single-board-arm-computer-with-bga/

Ahora mismo es un mundo excesivamente profesional e industrializado.

Exigir Hardware y software libre puede ser el primer paso.

Hay proyectos de ensamblado cercano con otro tipo de perfil.

#### i) Trazabilidad.[^](#51aplicamos-nuestros-criterios)

Nos referimos a que los materiales sean reciclados, o provengan de prospecciones
mineras éticas, no vengan de zonas de conflicto...

En equipos informáticos desconocemos proyectos que trabajen bajo ese criterio.

#### j) Amigabilidad y facilidad de uso.[^](#51aplicamos-nuestros-criterios)

La informática no es amigable, al final es lo que se llama una metáfora entre la persona y
el aparato eléctrico, y tendremos siempre que aprender a la forma que tiene de interactuar.

https://es.wikipedia.org/wiki/En_el_principio_fue_la_l%C3%ADnea_de_comandos

Lo que nos garantiza usar Software libre es aliviar la presión comercial de tener que recibir
siempre algo nuevo así como la obsolescencia programada, tenemos la ventaja de que
existen escritorios y aplicaciones informáticas que apenas han cambiado de aspecto
desde que se crearon originalmente.

Por ejemplo el escritorio MATE. Respecto al escritorio GNOME original.

Eso nos garantiza menos sobresaltos y cambios radicales en el tiempo.

Y siempre tendremos el intérprete de comandos :-D, con prácticamente los mismos
comandos desde sus inicios.

https://en.wikipedia.org/wiki/HandyLinux

Existe una comunidad potente de usuarias y desarrolladoras que estarán también siempre
ahí si necesitamos ayuda, sólo tenemos que preguntar.