[< 6.1. Criterios aplicados a la telefonía](./6_1_Criterios_telefonía.md)|[^ Índice](../README.md)|[> 6.3. Compendio de referencias (en telefonía)](./6_3_Referencias.md)

## 6.2.¡Caso práctico!

Necesitamos un teléfono y no nos planteamos estar sin uno.

Pensamos en un fairphone, pero descubrimos que nuestros padres tienen un BQ aquaris E5 que no usan porque dicen que está roto, a veces no se oía cuando llamaban y en la tienda le dijeron que necesitaban ya uno nuevo, que ese ya no tiene soporte.

Miramos en lineage OS y... voilá!

https://wiki.lineageos.org/devices/vegetalte

básicamente hay que instalarle un software para el modo recuperación, concretamente un software libre llamado TWRP. Para después, desde este, instalar la nueva ROM (el nuevo sistema operativo) descargada en el teléfono.

Los BQ usan la herramienta que ofrece Android de base en cualquier sistema GNU/Linux como Ubuntu llamada fastboot.

Siguiendo los pasos del wiki, con un poco de interés, pericia y práctica... Podemos tener el BQ aquaris E5 reusado, actualizado, sin aplicaciones google, y un poco más libre que antes.

Si queremos delegar, es cuestión de pedírselo a alguien que sepa, o bueno. Pagar por un teléfono lo más ético posible, como el Fairphone.

Decidamos lo que decidamos, finalmente habrá que ir a f-droid.org e instalar el APK que de ahí podemos descargarnos, será nuestro sustituto de google play de aquí al futuro, con aplicaciones sólo de sofware libre.