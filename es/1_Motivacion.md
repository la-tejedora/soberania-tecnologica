[^ Índice](../README.md)|[> 2. ¿Qué vamos a abarcar?](2_Que_vamos_a_abarcar.md)
# 1. Motivación.

Vivimos en un momento donde las nuevas tecnologías están presentes en el día a día de
las entidades de economía alternativa, llegando a ser básicas para su sostenibilidad y
correcto funcionamiento, además de que permiten una mayor participación de distintos
agentes.

Sin embargo, en ocasiones esas tecnologías no son coherentes con los principios de la
entidad y con los valores que defienden esas economías en general. Es complicado
trabajar fuera de las tecnologías dominantes ya que en la mayoría de los casos requiere
un tiempo y una energía de la que no se dispone.

Por ello, espacios como este son una oportunidad para trabajar, poco a poco, hacia un
**mayor conocimiento y coherencia** sobre la tecnología y una mayor democratización a fin
de cuentas, permitiéndonos a personas ajenas a priori con las luchas sociales en el
campo de la tecnología, acercarnos a ese mundo y **poder establecernos unos criterios**.

El planteamiento del taller casi empezó como una clase de Google Docs, y ha terminado
desdembocando en una guía de consumo de tecnología. Somos conscientes de que
hemos sacrificado utilidad práctica real sobretodo cara a un posible taller, por la
oportunidad de poder poner sobre la mesa el panorama actual real al que se aboca el
consumo y producción de tecnología intentando ofrecer ventanas de oportunidad donde
se puede arañar un resquicio de esperanza y coherencia.

Cuando hablamos de panorama REAL, nos referimos a una coherencia 100% en consumo
tecnológico, no un simple lavado de cara, o justificación (y autojustificación) de un
capitalismo verde o con rostro más humano.