[< 5.1. Criterios aplicados a los ordenadores](5_1_Criterios_ordenadores.md)|[^ Índice](../README.md)|[> 5.3 Compendio de referencias](./5_3_Referencias.md)

### 5.2.¡Caso práctico!

¡Necesitamos un equipo informático!

**¿Qué necesitamos?**

Queremos poder llevárnoslo por ahí para trabajar en cualquier sitio, y queremos que sea lo
más ético y ecológico posible.

Hemos mirado a nuestro alrededor y no hay ninguno para reciclar así que vemos qué
prestaciones queremos que tenga:

- equipo portátil, que me lo quiero llevar por ahí, y sé que consumen menos.
- pantalla pequeña, que sabemos que eso ahorra energía.
- ¿habrá alguno de hardware libre? ¡Me encantaría que fuera diseñado por una
    comunidad!
- No hace falta que sea muy potente pues es para leer el correo, editar texto,
    diagramas y algo de programación. 2-4 Gigas de RAM, varios núcleos, y
    preferiblemente 64 bits de arquitectura de la CPU. Si el disco duro fuera de estado
    sólido mejor, ¡claro, que es más rápido y consume menos electricidad!


- Yo no quiero tener que montar nada, que no tengo ni idea, a ver si rompo algo con la
    pasta que me gaste... Que venga ya preparado para trabajar.
- Voy a usar GNU/Linux, y si puedo GNU/Linux-libre como Trisquel, que sé que se
    empezó a desarrollar en Galicia, se basa en Ubuntu y protege aún más mi
    privacidad.

**Si priorizamos que tenga un diseño abierto, de hardware libre**

Lo primero que haríamos sería buscar un ordenador de hardware libre, con las referencias
que tengo no encuentro casi nada, y ¡además tienes que montarlo tú!

(buscamos en duckduckgo _hardware libre_ y _ordenador_ , o mejor en inglés, _open source
hardware computer_ , etc.)

https://en.wikipedia.org/wiki/Open-source_computing_hardware

https://www.crowdsupply.com/mnt/reform

https://www.olimex.com/Products/DIY-Laptop/KITS/

**Vale, sin diseño abierto, pero con software 100% libre, desde el sistema operativo hasta
la BIOS.**

La segunda opción entonces, es comprarlo con software totalmente libre desde tienda
aunque su electrónica no este licenciada como libre. Hay muchas famosas como: purism,
minifree, libquity, vikings, technoethical...

https://www.fsf.org/resources/hw/endorsement/respects-your-freedom

Todas ellas perfectas en coherencia de libertad pues ofrecen el ordenador con una BIOS
libre, garantizando el 100% de libertad en el software que vamos a utilizar. (purism no del
todo)

Por cercanía, que son europeos, voy a technoethical:

tienen el modelo: x200s (https://tehnoetic.com/laptops/tet-x200s) es un poco antiguo en
tecnología del procesador, pero está bien de RAM y tiene un disco duro de estado sólido,
pantalla de 12 pulgadas, etc. Para trabajar debe estar bien y entra dentro de nuestras
especificaciones.

**Con soporte al menos para software libre, si puede ser 100% libre. (recordad Trisquel VS
Ubuntu)**

Si por lo que fuera viéramos que son equipos demasiado viejos y/o caros, o simplemente
preferimos comprar un equipo portátil a un distribuidor más cercano o contemplamos la
opción de segunda mano en nuestro entorno...


Lo que deberíamos hacer es usar el nombre completo del equipo portátil, eso lo
conseguiríamos usando el nombre comercial que tiene, con el número de modelo que
suele venir en una pequeña etiqueta pegada en el revés del portátil. por poner dos
ejemplos:

- Tenemos posibilidad de reciclar un pequeño Asus Eeepc, si miro su revés veo que
    pone que es el modelo: **900HA**
- Tenemos posibilidad de comprar un HP Pavilion 15, si miro su revés veo que pone
    que es el modelo: **15-n058ss**

Lo primero que vamos a hacer es ir al portal h-node.org y buscar sus modelos por si
alguien de la comunidad ha analizado ese equipo bajo software libre.

Si en la página de h-node.org elegimos arriba la opción hardware, nos saldrán diversas
categorías de equipos electrónicos.

En la categoría netbooks, laptops, etc. Buscaremos equipos informáticos y su
compatibilidad.


La página web nos permite filtrar por vendedores, nivel de soporte, etc. Y posee también
un buscador, ahí será el lugar para poner todas las palabras clave que hemos obtenido en
nuestra búsqueda de información previa:

Si buscamos 900HA, resulta que el equipo aparece y ¡tiene soporte platinum! Estamos de
suerte.

Pero eso no suele ser lo común, pues es tan basta la cantidad de modelos distintos y
variantes que sacan los fabricantes, que aún habiendose hecho ya un trabajo encomiable
los equipos en h-node.org representan una minoría respecto al total.

Así si buscamos pavilion, pavilion 15 o directamente 15-n058ss no encontraremos nuestro
modelo en la página.


¿Qué hacemos? Lo único que nos queda es ir a la página del fabricante para ver las
especificaciones del equipo y ver qué chipset wifi tiene, qué tarjeta gráfica, etc. A la web
vamos a llegar buscando en duckduckgo el modelo directamente.

¡Lo malo es que en este caso (y es muy común) la web del fabricante no nos da detalles
como el chipset wifi! ¡Ay este imperialismo voraz que nos desinforma!

https://support.hp.com/us-en/document/c03975903

**wifi**

Pero estamos de suerte porque en el revés de este portátil en concreto viene qué chipset
wifi tiene. (no suele ser para nada lo normal)

El modelo de tarjeta de red es Atheros AR5B125 ( _Contains Atheros Model ARSB125_ ),
vamos a ver qué usa el tipo de chipset AR9485 haciendo una búsqueda en
_duckduckgo.com_ :

https://wikidevi.com/wiki/Atheros_AR5B125

Si busco en el wiki de GNU/Linux para dispositivos de red, sabemos que es Atheros y de la
rama ATH9k, ya que es un chipset modelo 9000:
https://wireless.wiki.kernel.org/en/users/drivers

Podemos ver que el driver ATH9k soportaría nuestro chipset AR9485 así que tenemos
certeza de su buen soporte por software solamente libre y GNU/Linux.


**Gráfica**

El punto flojo de este equipo será la tarjeta gráfica, que al ser AMD ( _AMD Radeon HD
8610G/8670M Dual GPU_ ) tiene todas las papeletas de no funcionar con todo su potencial.

Podemos intentar buscarlo en h-node.org a ver si vemos algo, seleccionando tarjetas
gráficas y filtrando por vendedor AMD.

Si no encontráramos nada, no deberíamos desesperar, al menos el equipo va a funcionar
aunque perdiendo algunas capacidades extra, como una resolución más flexible de
pantalla, aceleración 3D (para juegos, aplicaciones que necesiten de mucha tarea
gráfica...) Igualmente usando el blob privativo correspondiente
(https://en.wikipedia.org/wiki/Binary_blob) una distribución más laxa en términos de
libertad como Ubuntu debería poder funcionar a pleno rendimiento.

Podemos ya decidir si lo asumimos o no.