[< 7.2. Compendio de referencias (en software)](./7_2_Referencias.md)|[^ Índice](../README.md)|

## 8 ¿Tenemos una fábrica?

Hay alternativas a las convencionales ofrecidas por grandes multinacionales. Schneider,
Siemens, Omron, Wago, Lenze, Panasonic...

Opciones HMI y SCADA al alcance de la mano. HMI y SCADA son los equipos que van a
ser controlados directamente por las operarias en una planta, son ordenadores con un
programa de supervisión o pantallas empotradas.

Existen pantallas ya hechas, y se pueden construir con pocos componentes (SBC más la
pantalla y poco más)

Existen programas de software libre con capacidad de comunicación industriales que se
pueden programar para realizar la supervisión.

https://pvbrowser.de/pvbrowser/index.php

https://pyscada.readthedocs.io/en/dev-0.7.x/

Opciones de autómatas programables, los autómatas son los dispositivos que estarán en
cuadros eléctricos y actuarán con las máquinas y sensores de la planta directamente.

Los hay de hardware y software libre, gracias al _boom_ de los SBC. Hay proyectos de
software libre también como estándares en la programación de autómatas.


https://beremiz.org/

[http://www.openplcproject.com/](http://www.openplcproject.com/)
