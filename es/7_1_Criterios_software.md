[< 7. Vamos a utilizar una aplicación informática.](./7_Software.md)|[^ Índice](../README.md)|[> 7.2. Compendio de referencias (en software)](./7_2_Referencias.md)

## 7.1. Aplicamos nuestros criterios

#### a)Software libre

Cuando utilicemos aplicaciones informáticas es importante saber:

No hace falta que seamos personas doctas en la infinidad de licencias de software libre que hay, pero estaría genial saber si es una licencia libre aprobada por la FSF.

https://www.gnu.org/licenses/license-list.es.html

El concepto de software libre (4 libertades: estudiar como esta hecho, compartirlo, usarlo como se quiera, modificarlo)

Diferencia entre licencias copyleft y sin copyleft: ejemplo de Apache vs GPL.

https://en.wikipedia.org/wiki/Copyleft

Ejemplo del caso DarwinOS, licenciado bajo APSL una licencia sin un verdadero copyleft, es decir, sin autoprotección, que aún manteniendo las 4 libertades del software libre permite que Apple utilice el código en conjunción con software cerrado propio sin devolver nada a la comunidad, en su sistema operativo MacOS.

https://en.wikipedia.org/wiki/Darwin_%28operating_system%29

https://www.gnu.org/philosophy/apsl.html

Licenciado bajo GPL no sólo basta, como estándar debería dar la libertad de actualizar la licencia a una versión posterior: ejemplo de GPLv2+ ó _GPLV2 or as you wish any later version_.

Ejemplo del caso Linux, como núcleo, licenciado bajo la GPLv2 y sólo bajo esa versión de la licencia, que ha permitido huecos en su implementación para que la industria lo explote boicoteando las libertades que la licencia pretendía proteger.

https://es.wikipedia.org/wiki/Tivoizaci%C3%B3n

¿coincidencia? Hay muchos intereses de por medio.

También hay que tener en cuenta las licencias dependiendo del uso que se le va a dar a la aplicación, si vamos a utilizar una aplicación web, usando nuestro navegador de internet en conjunción con un servidor externo, es interesante reclamar que se licencien las aplicaciones bajo la AGPL (siempre con la posibilidad de autoactualización de licencia, AGPLv2+, AGPLv3+...)

https://en.wikipedia.org/wiki/Affero_General_Public_License


Esto nos garantizará que se publique el código de la aplicación que están ejecutando e implementando realmente, impidiendo que alguien pudiera modificar un programa de software libre, modificarlo, y servirlo en internet vulnerando el derecho de las usuarias a saber que están ejecutando y utilizando realmente.

#### b)Buena calidad (que cumpla las expectativas para mi trabajo a realizar)

La maravilla del software libre y el desarrollo en comunidad, es que hay una vasta red de alternativas de programas e implementaciones, que simplemente están ahí para que las probemos.

Si disponemos de una distribución GNU/Linux-libre o Linux, podemos usar los distintos gestores de paquetes (aplicaciones) que traen ya incorporados (apt, portage, etc.) y buscar y probar cuantas aplicaciones queramos.

Al ser la filosofía general de las distribuciones GNU/Linux, la de tener todo el sistema bajo control y centralizado, podemos instalar y desinstalar aplicaciones sin problema alguno, y sin miedo de _ensuciar_ nuestro sistema.

No vamos a hablar de las virtudes de GNU/Linux respecto a otros sistemas menos seguros, es simplemente deciros que probéis sin miedo! Leñe!

Podéis encontrar listados de aplicaciones fácilmente en la wikipedia, de muestra algunos ejemplos:

Distintos tipos de aplicaciones locales

- Sistemas operativos:
    GNU/Linux y preferiblemente GNU/Linux-libre, y más preferiblemente
    Libreboot/pmon/uboot... + GNU/Linux-libre.
    ◦ Opciones que alargarán la vida de nuestros equipos, incluso ahorrar batería.
       (desde SOs mínimos a escritorios o aplicaciones ligeras)
       Por ejemplo para un equipo medio, usar ubuntu-mate en vez de ubuntu.
       Para un ejemplo con un equipo muy antiguo es posible usar distribuciones como Antix. https://antixlinux.com/
- Ofimática:


```
libreoffice es muy buen proyecto, ¡pero pesa mucho! Abiword, gnumeric son
buenas alternativas para equipos lentos...
openoffice está menos desarrollado. (por temas de licencia Apache vs MPL, LGPL,
etc. lleva las de perder respecto a libreoffice)
```

- Audiovisuales
    Pitivi, Avidemux son famosos para la edición de video, Gimp es un clásico para el
    tratamiento de imágenes, Blender para el modelado en 3D... hay articulos de
    wikipedia para comparar y probar.
    https://en.wikipedia.org/wiki/Comparison_of_video_editing_software
    https://en.wikipedia.org/wiki/Comparison_of_free_software_for_audio
    https://en.wikipedia.org/wiki/Comparison_of_vector_graphics_editors
    https://en.wikipedia.org/wiki/Comparison_of_raster_graphics_editors
    ...

Aplicaciones cliente/Servidor

- Correo electrónico:
    clientes de correo electrónico (aplicaciones para ver el correo en el ordenador):
    Thunderbird,evolution,claws... K9mail, en el teléfono
    servidores de correo electrónico: nodo50.org / pangea / riseup / sindominio...
    https://en.wikipedia.org/wiki/Comparison_of_email_clients
    https://riseup.net/en/security/resources/radical-servers
- mensajería instantánea/videoconferencia:XMPP, ring, jitsi? (Problema con telegram,
    la aplicación cliente es software libre, el servidor no)
    problema con los programas de videoconferencia y mensajería, que solemos
    necesitar un servidor, ¿y pagamos algo por mantenerlo?
    https://en.wikipedia.org/wiki/Comparison_of_VoIP_software
    https://en.wikipedia.org/wiki/Comparison_of_instant_messaging_clients#XMPP-
    related_features
    https://en.wikipedia.org/wiki/Comparison_of_XMPP_server_software
- gestión financiera: dolibarr (cliente servidor), odoo (cliente servidor), civicrm
    (cliente servidor), GNU cash (aplicación local) ...


- gestión de proyectos, tareas, hitos...: dolibarr/odoo (ERPs)
    https://en.wikipedia.org/wiki/Comparison_of_project_management_software
    https://en.wikipedia.org/wiki/Comparison_of_accounting_software
- citas con dudle, tiene la aplicación funcionando la Universidad de Dresden.
    https://dudle.inf.tu-dresden.de/
- edición online eterpad/etercalc...
    https://en.wikipedia.org/wiki/Comparison_of_spreadsheet_software
    https://en.wikipedia.org/wiki/Comparison_of_word_processors
- compartición de archivos nextcloud/owncloud/seafile...
    https://en.wikipedia.org/wiki/Comparison_of_file_synchronization_software
    https://en.wikipedia.org/wiki/Comparison_of_file_hosting_services
- calendario y agenda compartida: los ERPs lo tienen (dolibarr, odoo, civicrm...) y
    nextcloud también, etc.
- difusión, redes sociales libres y listas de correo. Mailpoet es un plugin para web
    (wordpress, drupal)... Muy extendido, pero no está bien licenciado y tiene versión
    dual, desconozco alternativas.
    Siempre tendremos GNU _mailman para listas de correo, es un clásico._
    _redes sociales libres:_ Mastodon, GNUsocial, Friendi.ca,Pleroma, Diáspora. Hubzilla
- Desarrollo de una web propia. (usando gestores de contenidos wordpress, drupal,
    joomla) (hay que tener muchas cosas en cuenta, librejs, código externo
    (bootstrap?), etc.) (y donde se aloja? Quién lo mantiene?)
    https://en.wikipedia.org/wiki/List_of_content_management_systems
    https://www.gnu.org/software/librejs/

#### c)Bajo consumo de recursos (memoria y máquina necesaria para ejecutarla)

Es difícil establecer un criterio universal, pero varias pautas podrían ser:

- el lenguaje de programación, el tipo de lenguaje que utilicen las personas desarrolladoras influirá en la eficiencia del programa. Así hay lenguajes más fáciles de desarrollar pero a alto nivel (tienen menos control de los recursos y de la eficiencia) y lenguajes más densos y de bajo nivel.


```
Así un programa en ensamblador teóricamente debería ser más eficiente que uno
en C, uno en C más eficiente que uno en C++, y uno en C++ más eficiente que en
Python...
No tiene porque ser así, dependerá también mucho de la habilidad y buen hacer de
las personas desarrolladoras. Pero puede ser un punto a tener en cuenta buscando
aplicaciones. Esa información suele venir en la wikipedia.
```

- La interfaz gráfica influirá también, es decir, como se comunica con nosotras la aplicación. Hay librerías gráficas mucho más eficientes y sencillas que otras. GTK y QT son pesadas, y son las más utilizadas. Una aplicación de terminal será más eficiente y rápida por norma general.

¡La vistosidad y efectos siempre consume recursos!

- Dicho esto lo suyo es probar, hay siempre alternativas a un mismo tipo de programa. Así tenemos el Libreoffice Writer para documentos de texto, pero hay alernativas menos pesadas como el Abiword. Probad y ver como responde en velocidad y prestaciones.

#### d)Que esté bien mantenida.

Que una aplicación esté bien mantenida resulta básico cara a estar actualizada en temas de seguridad y nuevas funcionalidades.

En aplicaciones que vayamos a usar en nuestro escritorio y no tengan conexión con el _exterior_ , no usen datos de internet, puede ser aceptable utilizar versiones antiguas.

En aplicaciones en nuestro teléfono móvil, que use comunicaciones con _el exterior_ , o que haya que instalar en un servidor pediremos que se actualicen a menudo.

Actualizar a menudo una aplicación es buena seña de salud, nos informa que arreglan posibles agujeros de seguridad que vayan sucediendo y se adaptan a nuevas versiones de librerías y/o herramientas que las aplicaciones en sí necesitan.

Que un programa tenga buena salud en sus versiones nos vendrá fácilmente en la wikipedia.

Por ejemplo, estamos dudando entre las diversas aplicaciones existentes en GNU/Linux para edición de videos (montaje, edición de fotogramas, etc.)

Entre las opciones que nos han comentado hemos barajado dos, una Kino, y otra Avidemux.

Vemos en la wikipedia que nos informan de la última versión estable de cada una de estas aplicaciones, y Kino lleva ya 9 años sin sacar una nueva versión. No debería considerarse así una opción para instalar.

https://en.wikipedia.org/wiki/Avidemux

https://en.wikipedia.org/wiki/Kino_(software)

#### e)Buena política comunitaria y de desarrollo.

Este aspecto es muy importante también, pues deberíamos fomentar o apoyar proyectos que fomenten la colectividad, tengan una política transparente y participativa.

Aquí deberíamos valorar la facilidad de recibir soporte, el trato recibido, la riqueza de la comunidad...

Vamos a dar varios ejemplos:

- El proyecto Owncloud es software libre, es una nube que se puede instalar y mantener en un servidor y ofrecer servicios de almacenaje de archivos, compartición de calendarios, edición colaborativa de documentos, etc. Owncloud seguía una filosofía de tener una versión libre y otra _empresarial_ , con modificaciones y añadidos cerrados.
 Esta filosofía de dudosa éticidad, pues existe el debate del uso de la comunidad para un beneficio privativo posterior, llevó a una escisión (un fork) del proyecto donde nació otro llamado nextcloud que sólo contempla una versión única y totalmente libre de la aplicación.

- El proyecto OpenOffice es software libre, bajo una licencia laxa y sin protección, era el referente como paquete ofimático de software libre hasta que la empresa que estaba detrás de su desarrollo y mantenía el copyright de la aplicación, fue adquirida por un gigante de la informática, famoso por su política pro software cerrado y acaparamiento de conocimiento. 
En el momento de la compra se formó una escisión (un fork) llamada LibreOffice con una fundación detrás, una licencia de software libre con protección, y una comunidad de usuarias y gente desarrolladora potente. Hoy en día es un referente.

¿Como valorar esto entonces? Las recomendaciones podrían pasar por:

- qué entidad posee el copyright y/o está detrás de impulsar el proyecto, multinacional, Pyme, fundación...

- ¿el proyecto pone facilidades a la participación de las usuarias? ¿hay foros y con actividad? ¿el trato es correcto?

- ¿el proyecto hace apología del software libre como filosofía?¿o más bien tiene un corte más empresarial donde se concentra más en la solución a un problema que en el medio empleado?

- ¿el proyecto está bien documentado? ¿hay buenas guías de uso y trabajo?

#### f)Que sea segura en relación a mi privacidad.

Por supuesto damos por sentado tal axioma universal que el software libre es vital si buscamos seguridad y preservar nuestra privacidad.

https://es.wikipedia.org/wiki/Seguridad_por_oscuridad#Argumentos_contra_la_seguridad
_por_oscuridad

Pero eso no basta, habría que tener en cuenta más criterios:

- Valorar el software libre, y cuando decimos libre queremos decir 100% libre. Está bien que empecemos poco a poco, pero debemos ser conscientes de que una distribución GNU/Linux Ubuntu en un ordenador y un teléfono Android, no son sistemas 100% libres. 
Esto tiene muchas implicaciones pues va a haber partes claves del sistema que van a apoyarse en código cerrado para funcionar, y eso, son brechas de seguridad. 

Si el microcódigo que permite usar un wifi es cerrado, estamos comprometiendo nuestras comunicaciones, si al GPS igual, estamos comprometiendo nuestra geolocalización...

Debemos cuestionarnos y demandar desde una electrónica a unas plataformas software **totalmente libres**. Proyectos como Libreboot, Trisquel, Replicant, la certificación de la FSF, etc. Están ahí defendiéndonos.

- Cuando instalamos programas no instalamos su código, si no que este ha sido preparado en forma de binario para poder usarse en nuestro sistema correctamente.

https://es.wikipedia.org/wiki/Archivo_binario

Imaginad por ejemplo el equipo que programa el libreoffice, esta gente trabaja en el código fuente de la aplicación, pero esta luego debe ser preparada para trabajar bajo Ubuntu GNU/Linux, para MS Windows, para FreeBSD... Para ordenadores de 64 bits, de 32 bits, etc.

Esta preparación sería fruto de compilar , es decir, preparar ese código para dejarlo listo para trabajar en la plataforma deseada. Así es super importante tener en cuenta de donde y como obtenemos el binario preparado para funcionar porque este no es transparente como el código, y verdaderamente no sabemos qué
ejecutamos.

Así, si no podemos compilarnos nuestro propio código (hay distribuciones GNU/Linux como gentoo y varios sistemas más que compilan los programas en vez de instalar los binarios directamente, pero suelen ser bastante más complejas de
usar) debemos usar siempre los repositorios oficiales de nuestra distribución, y a poder ser 100% software libre. Esto nos garantiza que hay una comunidad con personas velando por los binarios que se suministran, y el sistema corrobora mediante firma digital de donde instalamos las aplicaciones (para que nadie se pudiera hacer pasar por nuestra comunidad) Si eso no pudiera ser deberíamos acudir a la página oficial del proyecto que deseamos, y comprobar la firma digital del binario que descargamos si nos dan la posibilidad.

Incluir repositorios externos en nuestro sistema GNU/Linux es a todas luces una maniobra insegura, deberíamos al menos tener confianza en el equipo desarrollador y mantenedor del repositorio externo.

https://es.wikipedia.org/wiki/Repositorio_de_software


- Deberíamos también tener una cultura en privacidad y seguridad bien adquirida. Uso de contraseñas complejas, uso de encriptación de correos, reducción en nuestra exposición pública, uso de las redes sociales, uso de google, amazon, etc. (donde tenemos cuenta y con quién?), etc.

- Finalmente y por profundizar más, hay gente y colectivos que trabajan con más detenimiento la temática de la privacidad y la seguridad informática. Puede ser más que recomendable una formación específica pues es un tema problemático y demasiado actual.

#### g)En caso de necesitar un servidor, ¿quién y como lo implementa?

De forma cotidiana utilizamos aplicaciones informática que necesitan de un equipo servidor para la gestión del servicio que ofrecen. Así cuando usamos el correo electrónico, leemos las noticias en nuestro navegador web, mensajería, redes sociales, compramos...

Nuestra información _escapa a nuestro control_ desde la seguridad de nuestro equipo informático al exterior.

Estamos educados a su vez en una cultura de hacer invisible ese equipos informáticos que están en centros de datos enormes, ese trabajo que conlleva su mantenimiento, esa importancia que tiene como se gestionan, con qué transparencia, y la importancia que tiene el ingente volumen de datos que trabajan.

Para valorar una solución coherente habría así que tener en cuenta varios aspectos:

- Que sea software libre evidentemente, aunque aquí ocupa una nueva dimensión pues podemos encontrar el vacío legal de una aplicación libre modificada por una gran empresa de forma privada, y servida en un servidor.

Las licencias clásicas de software libre estaban pensadas para aplicaciones de escritorio, así que una modificación de código para un uso privado no obliga a la publicación del código modificado. Una gran empresa podría así modificar una aplicación en un servidor y no publicar los cambios, alegando que la usa para un uso privado.
    
Para ello se desarrolló la licencia Affero GPL (AGPL), es una licencia libre que obliga a las aplicaciones web, a publicar el código que están ejecutando. Deberíamos así fomentar el uso de este tipo de aplicaciones cuando usemos aplicaciones que se basan en su instalación en un equipo servidor. (por ejemplo, https://decide.madrid.es)

- ¿Quién gestiona ese equipo? ¿Podemos fiarnos igual de una multinacional que hace lobbying que de una asociación o colectivo social? ¿Es igual una PYME que una cooperativa? ¿En manos de quién ponemos nuestra información? ¿Qué nivel de transparencia y participación ofrecen?

- ¿Qué modelo de negocio explota la entidad que sirve el producto? ¿Porque una gran multinacional ofrece correo, edición de documentos y almacenaje de 15Gb de forma gratuita? ¿Cuanto nos costaría ese servicio con por ejemplo Nodo50 o con Pangea, que es el colectivo en el cual basa su servicio de alojamiento web coherente el Mercao Social de la Tejedora?

Mantener un servidor requiere mucho trabajo e infraestructura, tiene que estar siempre funcionando pues no nos imaginamos ya un día sin tener el correo electrónico operativo, ¿verdad?


```
Desde una visión de la Economías Transformadoras es impensable que no queramos remunerar este esfuerzo de forma justa.

Lo contrario sería ser carnaza fácil de una invasión flagrante de la vida privada, sabrán qué nos gusta, qué nos inquieta, nuestra ideología... Y lo peor de todo, nos manipularán. De eso no cabe duda. Por eso no paran de crecer estas grandes empresas, porque la información es poder.
```

#### h)Amigabilidad y facilidad de uso.

Es una filosofía clásica de las grandes compañías de software cerrado, e incluso lo estamos viendo en algunas que ofrecen productos de software libre en la actualidad.

Una forma de enganchar al cliente, es revolucionar su aplicación o sistema, haciéndoles que compren el nuevo, o el servicio de actualización, etc.

Te enganchan a consumir y a aprender nuevas formas de hacer lo mismo que ya estabas haciendo años atrás.

Unas pautas que podemos dar para garantizar los menos sobresaltos en nuestra relación con la informática, sin sacrificar estar a la última en seguridad, podrían ser:

- Evidentemente usar software libre, los proyectos de software libre son potentes en cuanto a seguridad y arreglo de fallos, pero son menos arriesgados a la hora de revolucionarnos la experiencia de usuaria. Esto se debe a que no tienen la presión de vender un producto, de tener que crecer y crecer hasta el infinito como modelo de negocio.
    
El movimiento del software libre nació con una premisa que ha condicionado todo su desarrollo posterior. El éxito de una aplicación informática no se mide por la cantidad de usuarias que lo usen, si no por lo bien programado, licenciado y publicado que esté, y lo libre que sea. La idea es hacer comunidad, no ser famoso.

Podemos estar _toda la vida_ usando el escritorio para GNU/Linux Gnome2, ahora en desarrollo por el proyecto Mate, que desde el 2002 ha mantenido el mismo aspecto y funcionalidad.

- Seguir la filosofía de desarrollo de un proyecto. A día de hoy hay también muchos proyectos incluso de software libre que mantienen una política de actualizaciones y de retro-compatibilidad (compatibilidad con versiones anteriores) muy agresivas.
    
Deberíamos valorar esto a la hora de elegir una aplicación informática, cada cuando se actualiza, es compatible con la anterior ¿o tengo que invertir trabajo/dinero en actualizar? ¿Ofrece mecanismos fáciles de actualización? ¿Tengo que volver a aprender algo nuevo? ¿Se justifican los cambios o son meramente estéticos? ...

- ¿Qué comunidad está detrás? ¿Hay buena documentación? ¿Es fácil conseguir ayuda?