[< 5.2. Caso Práctico)](./5_2_Caso_practico.md)|[^ Índice](../README.md)|[> 6. Adquirimos telefonía](./6_Telefonía.md)

### 5.3.Compendio de referencias de proyectos.

Proyectos para pedir ayuda y buscar información:

La wikipedia es maravillosa para artículos técnicos

https://en.wikipedia.org/wiki/Light-weight_Linux_distribution

https://en.wikipedia.org/wiki/List_of_open-source_hardware_projects

https://en.wikipedia.org/wiki/Free_and_open-source_graphics_device_driver

...

preguntar por programas, alternativas y ayuda:

ask ubuntu:

https://askubuntu.com/

debian forum:

[http://forums.debian.net/](http://forums.debian.net/)

trisquel forum:

https://trisquel.info/en/forum

...

Comunidades y proyectos comunitarios:


H-node (https://h-node.org) → base de datos de dispositivos y su soporte bajo GNU/Linux-
libre.

FSF (https://www.fsf.org/resources/hw/endorsement) → proyecto de la Free Software
Foundation, que está empezando a acreditar a colectivos y dispositivos que respetan al
100% la libertad de los usuarios. (pueden funcionar con sólo software libre)

libreboot (https://libreboot.org/) proyecto de bios libre principalmente para equipos x86 →
y amd64.

Trisquel (https://trisquel.info/) distribución 100% libre, de origen Gallego y basada en →
ubuntu.

Drivers libres para chipsets wifi en GNU/Linux
(https://wireless.wiki.kernel.org/en/users/drivers) usémoslo de referencia para saber si →
un dispositivo wifi tiene soporte o no con software libre.

Drivers libres para Tarjetas gráficas en GNU/Linux
(https://en.wikipedia.org/wiki/Free_and_open-
source_graphics_device_driver#Free_and_open-source_drivers) Nótese que los drivers →
para gráficas AMD y Nvidia no suelen ser totalmente libres pues cargan microcódigo
cerrado en el núcleo.

Linux-sunxi (https://linux-sunxi.org) comunidad que trabaja para el buen soporte de la→
rama principal del núcleo Linux de los equipos basados en procesadores ARM Allwinner.

Armbian (https://www.armbian.com/) → comunidad que trabaja y ofrece distribuciones
Debian y Ubuntu normalmente basadas en la rama oficial de GNU/Linux para SBCs con
microprocesadores ARM.

Proyectos interesantes:

Teres-I (https://www.olimex.com/Products/DIY-Laptop/) → portátil basado en hardware
libre totalmente, de ensamblado manual, aún en pruebas, por olimex.

Proyecto eoma68 (https://www.crowdsupply.com/eoma68) aún en prototipado, es un →
ordenador que aspira a estar bajo el sello de la FSF, intenta ser reparable y bastante
modular.

Proyecto Portátil MNT Reform (http://mntmn.com/reform/) Es bastante nuevo y parece →
ser aún un prototipo, pero tiene buena pinta, es un portátil para hacer uno mismo, con
énfasis en el hardware y software libre y la privacidad.

Distribuidores, ensambladores, empresas desarrolladoras...:

TechnoEthical (https://tehnoetic.com/)→ tienda online que reacondiciona dispositivos y
los vende garantizando un 100% de respeto a la libertad.


Olimex (https://www.olimex.com) → empresa búlgara de desarrollo de placas y
dispositivos varios basados en hardware libre. Participa activamente en Linux-sunxi.

Purism (https://puri.sm) → se denominan como corporación de propósito social,
desarrolla portátiles con el intel ME deshabilitado con coreboot, y una versión de
GNU/Linux-libre. También telefonía.

Distribuidores de ordenadores genéricos con GNU/Linux, cercanos:

vantpc.es (https://vantpc.es) distribuidor Local con GNU/Linux. →

Slimbook (https://slimbook.es/) ídem que vantpc, quizá con más nombre e historia. →

mountainpc (https://www.mountain.es/en) dicen ser fabricantes...Pero yo creo que son →
distribuidores. De gama alta. (no lo he probado) (antes decían que instalaban GNU/Linux...
Ya no veo nada)

Tiendas de segunda mano:

la Asociación Madre Coraje ( [http://segundamano.madrecoraje.org)](http://segundamano.madrecoraje.org)) →

tiene una tienda de electrodomésticos y ordenadores reciclados.

En Córdoba También tenemos el centro Reto. Etc.

Koopera en Euskadi, con la reutilización y reciclaje de electrodomésticos
(https://www.koopera.org/)
