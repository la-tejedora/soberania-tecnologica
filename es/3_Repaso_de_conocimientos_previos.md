[< 2. ¿Qué vamos a abarcar?](2_Que_vamos_a_abarcar.md)|[^ Índice](../README.md)|[> 4. Criterios que pueden regir nuestra elección: (hagamos un sondeo!!)](4_Criterios.md)
# 3. Repaso de conocimientos previos técnicos.

### Debemos saber de qué hablamos cuando decimos **software** y **hardware**.

+ **Hardware** es la parte física del ordenador (teléfono, cacharro, etc.) circuitos electrónicos, placas, etc. (https://es.wikipedia.org/wiki/Hardware)[https://es.wikipedia.org/wiki/Hardware]

+ **Software** es la parte programada, es el código que la persona técnica ha insertado para que la electrónica realice una función determinada. El código es texto con una lógica determinada que se compila o interpreta para que la parte física (hardware) lo ejecute. (https://es.wikipedia.org/wiki/Software)[https://es.wikipedia.org/wiki/Software]

+ El **software libre** es un movimiento político que nace en los años 80, y proteje 4 libertades del software: libertad de usarlo como se quiera, de estudiarlo, de compartirlo y de mejorarlo.

+ El **hardware libre** es un movimiento más moderno y no tan articulado y coherente, que básicamente pretende las mismas libertades del software en componentes y circuitos electrónicos

### Componentes de un equipo informático y opciones. (CPU/APU/GPU, RAM, disco duro...)

+ (https://es.wikipedia.org/wiki/Unidad_central_de_procesamiento)[https://es.wikipedia.org/wiki/Unidad_central_de_procesamiento]
+ (https://es.wikipedia.org/wiki/AMD_Accelerated_Processing_Unit)[https://es.wikipedia.org/wiki/AMD_Accelerated_Processing_Unit]
+ (https://es.wikipedia.org/wiki/Unidad_de_procesamiento_gr%C3%A1fico)[https://es.wikipedia.org/wiki/Unidad_de_procesamiento_gr%C3%A1fico]
+ (https://es.wikipedia.org/wiki/Memoria_de_acceso_aleatorio)[https://es.wikipedia.org/wiki/Memoria_de_acceso_aleatorio]

### conceptos más modernos de equipos informáticos y chips como (SBC,SOCs)

+ (https://es.wikipedia.org/wiki/Placa_computadora)[https://es.wikipedia.org/wiki/Placa_computadora]
+ (https://es.wikipedia.org/wiki/System_on_a_chip)[https://es.wikipedia.org/wiki/System_on_a_chip]

Tipos de ordenadores disponibles (minipcs, sobremesa, portátiles...)

Arquitecturas de microprocesadores (x86, amd64, arm, arm64...mipsel...)

¡usad wikipedia, en temas técnicos es muy útil!

### concepto de nube y trabajo en red

+  Equipo servidor. (https://es.wikipedia.org/wiki/Servidor)[https://es.wikipedia.org/wiki/Servidor]

+ Relación de las aplicaciones informáticas tipo cliente/servidor.
    
    - p.e: la plataforma decide.madrid.es, el cliente es tu navegador de internet, el servidor ejecuta la aplicación consul, software libre, mantenida y configurada por las personas técnicas del ayuntamiento. El navegador puede ser software libre o no, dependiendo del que usemos, la aplicación que hace de servidora es software libre.(Consul)
    - p.e: whatsapp, usas el cliente en tu teléfono móvil, mandas y recibes mensajes, que pasan a través del equipo servidor desarrollado, configurado y mantenido por el personal de whatsapp. El cliente es software privativo (al aplicación de Whatsapp) la aplicación en el servidor es también software privativo.
    - p.e: Telegram, usas el cliente en tu teléfono móvil, mandas y recibes mensajes, que pasan a través del equipo servidor desarrollado, configurado y mantenido por el personal de whatsapp. El cliente es software libre (la aplicación de Telegram es software libre aunque con matices) la aplicación en el servidor es software privativo propio de la compañía.