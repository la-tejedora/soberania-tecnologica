[< 6. En la adquisición de telefonía.](./6_Telefonía.md)|[^ Índice](../README.md)|[> 6.2. !Caso Práctico¡ (En telefonía)](./6_2_Caso_practico.md)

## 6.1. Aplicamos nuestros criterios

### a) Que sea de buena calidad. [^](#61aplicamos-nuestros-criterios)

Las prestaciones técnicas a tener en cuenta de un teléfono serían:

#### Procesador:

Varios núcleos, más velocidad, etc.

Al igual que con los ordenadores es posible cotejando varias páginas ver el modelo exacto
de procesador y compararlo en la wikipedia. (velocidad, núcleos, consumo...)

#### Ram:

A partir de 1 Gb. (para un móvil Android fluido)

#### Disco Duro:

Es la memoria interna del teléfono, 8Gb, 16Gb... Consideremos comprar una tarjeta
microSd y acoplarlas si necesitamos más espacio.

#### Modelo del teléfono:

Sería interesante conocer el modelo del teléfono, o por la información en tienda o por la
que podamos sacar del teléfono en sí. (carcasa, o abriéndolo y detrás de la batería...)(por
ejemplo BQ E4.5 (en la parte posterior de la carcasa) , o Samsung SM-J100H (quitando la
batería))

Podemos ya, con el modelo del teléfono, sacar una descripción más afinada del hardware
en portales dedicados:

https://www.ifixit.com

Una posible garantía cara a mayor durabilidad y soporte es comprobar que sea un modelo
mantenido por comunidades como replicant, lineageOs, ubports... Eso nos da muestra de
que hay mucha gente que lo usa.


### b) Consumo eléctrico. [^](#61aplicamos-nuestros-criterios)

Sí compramos un teléfono nuevo sería interesante:

+ ver el consumo del procesadores y tenerlo en cuenta.
+ la pantalla cuanto más pequeña mejor.
+ batería extraíble.

### c) Posibilidades de reparación y reutilización. [^](#61aplicamos-nuestros-criterios)

Podemos plantearnos como primera opción no tener teléfono :-)

Si nos decidimos por **reutilizar** :

+ ver si existen recambios aún. (quizá algún portal o tienda de segunda mano o preguntando en una tienda de reparaciones...)
+ ¿se puede actualizar? Ver si lineageOS tiene versiones para ese modelo. (por ejemplo, también hay desarrolladores independientes como los del portal xda developers, pero habría que asegurarse que el código sea público, etc.)
+ Para la gran mayoría de programas android, basta con tener la versión 4 o superior, podemos instalarle f-droid y funcionar.

Teniendo en cuenta la **reparabilidad** , existen ya en el mercado teléfonos móviles con una filosofía de reparación y personalización mayor que otros. Por ejemplo, cambiar una pantalla en el Fairphone es infinitamente mucho más fácil que en otros equipos.

Que la batería sea fácilmente extraíble es otro punto a favor, ya que suele ser lo que más se rompe, y así garantizamos que podamos nosotras mismas cambiarla. Esto resulta cada vez más complicado.

Merece la pena, como ya hemos dicho antes, adquirir un dispositivo con futuro, eso es, que:

El nuevo terminaladquirido venga debidamente actualizado, podemos revisar las versiones de Android por ejemplo en la wikipedia: [https://es.wikipedia.org/wiki/Anexo:Historial_de_versiones_de_Android](https://es.wikipedia.org/wiki/Anexo:Historial_de_versiones_de_Android). **Adquirir un dispositivo con un sistema operativo obsoleto ya es de base una mala decisión**.

O que el dispositivo sea objetivo de las diversas comunidades de gente desarrollando herramientas y software libre para ellos. Lo explicamos mejor:

+ Que tenga versión del sistema de recuperación de software libre twrp: [https://twrp.me/Devices/](https://twrp.me/Devices/)
+ Que sea un objetivo prioritario para proyectos como replicant, lineageOs, o ubports
    - [https://redmine.replicant.us/projects/replicant/wiki/ReplicantStatus#Replicant-60](https://redmine.replicant.us/projects/replicant/wiki/ReplicantStatus#Replicant-60)
    - [https://wiki.lineageos.org/devices/](https://wiki.lineageos.org/devices/)
    - [https://devices.ubuntu-touch.io/](https://devices.ubuntu-touch.io/)
+ Que tenga guías y documentación en portales como https://www.ifixit.com , esto garantiza lo _famoso_ que pueda ser el dispositivo.

Esto nos facilitará el acceso a repuestos, y actualizaciones de android. Una vida a fin de
cuentas, más larga para nuestro dispositivo.

### d) Proximidad. [^](#61aplicamos-nuestros-criterios)

Hay proyectos de diseño de los equipos en Europa, pero ¿y ensamblado? ¿Y fabricado?

Hay que indagar más.

### e) Soportable para software libre, drivers. [^](#61aplicamos-nuestros-criterios)

El **único** proyecto de software libre en telefonía, y subrayamos el único porque ni Android, ni Firefox Os, ni Lineage OS, ni Ubuntu Touch... Son totalmente libres pues insertan partes de código cerradas ofrecidas por los fabricantes para que todo funcione _correctamente._ Es replicant.

[https://replicant.us](https://replicant.us)

Además es un proyecto que hace especial hincapié en la privacidad y tiene un transfondo muy didáctico. Ofrece guías de criterios a la hora de elegir un dispositivo móvil, etc.

La pena es que si el mundo de la informática es industrial, al menos este vino de comienzos más románticos y artesanales, el de la telefonía entró de lleno en la industria tecnológica del s. XXI así que las comunidades andan desde el principio totalmente a
remolque de lo que las grandes multinacionales ofrezcan en electrónica.

Así podemos ver en el listado de dispositivos que replicant oficialmente soporta como adolecen de cierta funcionalidad: aceleración 3D, wifi, GPS, bluetooth...

Si aún así queremos apoyar esta causa existen tiendas que venden terminales convencionales, que han sido adaptados con replicant instalado:

[https://tehnoetic.com/](https://tehnoetic.com/)

Se habla, se dice, se comenta... :-) que Google piensa dejar android por el sistema ChromeOS basado también en Linux que lleva años desarrollando. En la comunidad de software libre hablan también de abandonar así Android y abogar por que el escritorio GNU/Linux clásico pueda cubrir también las demandas de telefonía.

Existen ya proyectos para ejecutar aplicaciones Android en escritorio muy maduros.

[https://wiki.debian.org/Mobile](https://wiki.debian.org/Mobile)

### f) Diseño abierto y replicable (hardware libre!) [^](#61aplicamos-nuestros-criterios)

Bajo este criterio queremos encontrar un dispositivo móvil cuyas especificaciones, planos, y a poder ser componentes sean licenciados bajo una licencia libre, hecho que va a garantizar que pueda ser fabricado por cualquier entidad en cualquier parte del mundo.

En el mercado existen pocos o casi ningún ejemplo, que conozcamos el proyecto GTA04 es el único que cumple 100% con un criterio de diseño abierto.

[http://wiki.openmoko.org/wiki/GTA04](http://wiki.openmoko.org/wiki/GTA04)

Están surgiendo proyectos que intentan construir un teléfono móvil con componentes fáciles de adquirir en el mercado. Aún así, están alejados de un _smartphone_ al uso, y no se les podría instalar Replicant o Android para poder usar las aplicaciones típicas.

[https://wiki.zerophone.org/index.php/Main_Page](https://wiki.zerophone.org/index.php/Main_Page)

[http://www.davidhunt.ie/piphone-a-raspberry-pi-based-smartphone/](http://www.davidhunt.ie/piphone-a-raspberry-pi-based-smartphone/)

### g) privacidad. [^](#61aplicamos-nuestros-criterios)

Lo ideal sería un sistema de hardware libre, si no, uno que funcione 100% con software libre.

Si queremos hacer mucho hincapié en ello la opción ideal sería un terminal adquirido con Replicant.

En terminales más _convencionales_ , sería interesante tenerlo instalado con una distribución Android basada en una comunidad de forma oficial. (LineageOs? Y TWRP)

Y por supuesto **no uses google play!** De hecho no tienes porqué registrarte con Google.
Tienes **F-droid** [https://f-droid.org](f-droid.org):

##### 1) tienes que activar en ajustes de android: Permitir la instalación de aplicaciones de origen desconocido, que está en el apartado de seguridad.
##### 2) descargas el .apk de la página web, f-droid.org.
##### 3) lo ejecutas con el instalador de aplicaciones de Android.

*(estas instrucciones pueden cambiar dependiendo de la versión de Android)

Así garantizamos que son de software libre y están auditadas.

A parte evidentemente, tener buena cultura en seguridad. (Hay infinidad de manuales sobre el tema)

El proyecto purism, ofrece un proyecto que dice hacer especial hincapié en la privacidad, llamado librem 5. El hardware para empezar tiene algunas fallas en la seguridad (replicant dixit) pero puede merecer la pena echarle un ojo.

[https://puri.sm/products/librem-5/](https://puri.sm/products/librem-5/)

Existen en desarrollo otros sistemas operativos alternativos a android y libres en el papel, que sería interesante seguir: ejemplo de **librem** y **postmarketOS**.

### h) Economía Social, condiciones laborales justas, horizontalidad? [^](#61aplicamos-nuestros-criterios)

Habría que indagar más sobre el tema, en cualquier caso, cualquier teléfono comprado a pequeñas empresas que tienen cierta sensibilidad en libertades respecto a la tecnología, nos puede garantizar una cadena de producción un poco más justa.

(Fairphone, GTA04, technoethical...)

### i) Trazabilidad. [^](#61aplicamos-nuestros-criterios)

Nos referimos a que los materiales sean reciclados, o provengan de prospecciones mineras éticas, no vengan de zonas de conflicto...

El único proyecto que ha nombrado explícitamente el tema, es el proyecto Fairphone, un teléfono que tiene una fundación detrás, hace trazabilidad de materiales, explica bien lo que venden, especificaciones, etc.

[https://www.fairphone.com/en/](https://www.fairphone.com/en/)

Os invito a buscar más.

Problema de las multinacionales, hay muy pocos fabricantes de informática.

Al final Fairphone sólo podrá pedir explicaciones comerciales a sus proveedores, pero escapa a su control la tecnología usada pues deberá elegir entre las opciones que la industria le da si quiere ser competitivo.

### j) Amigabilidad y facilidad de uso. [^](#61aplicamos-nuestros-criterios)

En uso, el software libre siempre tiene un fin más didáctico con documentación pública,
tutoriales, etc.

El software cerrado y la industria suele querer que tomes los equipos informática como meros electrodomésticos (obviando que manejan información sensible), donde cuando se rompa llames al equipo técnico y pagues, o finalmente compres algo nuevo.

En reparación, Fairphone lo pone muy fácil, otros equipos como BQ son mucho más complicados. Portales de internet como ifixit hacen un gran trabajo recopilando manuales de reparación.
