[< 1. Motivación](1_Motivacion.md)|[^ Índice](../README.md)|[> 3. Repaso de conocimientos previos técnicos](3_Repaso_de_conocimientos_previos.md)
# 2. ¿Qué vamos a abarcar?

Vamos a aclarar un par de tecnicismos de forma superficial, que será necesario entender
para ver el alcance de nuestras decisiones a la hora de elegir determinadas tecnologías.

Después, juntas, vamos a establecer los criterios que deberían regir la elección y consumo
de las tecnologías que usamos, basando esos criterios en los que pediríamos a cualquier
otro producto cotidiano (alimentación, ropa...)

Vamos a aplicar esos criterios a las necesidades tecnológicas cotidianas personales y de
nuestros colectivos sociales y entidades, y enlazarlos a proyectos trabajando en ese
campo ya existentes, y alternativas que veremos que hay (o no)

El mundo tecnológico es de grandes corporaciones, es de fábricas muy especializadas y
deslocalizadas y un uso devastador de los recursos naturales del planeta, no decidimos
qué política o qué rumbo seguirá. Vamos a llegar hasta donde podamos, y como
militantes por una Economía Social y Solidaria y un modelo transformador, es nuestra
obligación exigir al mercado, y apoyar iniciativas próximas.

El enfoque es que sea práctico para una solución a corto plazo, sin perder la perspectiva
de los retos reales del mundo tecnológico, las libertades individuales, de la sociedad, su
democratización, y evidentemente los límites del planeta.

Como punto de partida sería interesante ser conscientes de que hay que evitar la
tecnología en la medida que sea posible, nos han inculcado el mito de que todo lo arregla,
y por ejemplo es infinitamente más importante saber trabajar en equipo y encontrar el
tiempo de calidad para dedicarnos a ello, que tener una maravillosa herramienta
informática de última generación para editar documentos online.