[< 6.2. !Caso Práctico¡ (En telefonía)](./6_2_Caso_practico.md)|[^ Índice](../README.md)|[> 7. Vamos a utilizar una aplicación informática](./7_Software.md)

### 6.3.Compendio de referencias de proyectos.

#### Proyectos para pedir ayuda:

como siempre...Usen la wikipedia :-)

foros de Replicant (https://www.replicant.us/)

foro xda-developers, fueron los pioneros en personalizar smartphones (https://www.xda-developers.com/)

...

#### Proyectos comunitarios:

La comunidad de Debian GNU/Linux está recopilando información para ser portado a teléfonos móviles, (https://wiki.debian.org/Mobile)

Hay una versión de ubuntu para móviles, personalmente creo que no aporta nada pues tiene los mismos problemas de software cerrado y seguridad que Android. Pero es buena fuente para ver teléfonos móviles a los que se les puede cambiar el sistema operativo. (https://ubports.com/es_ES/)

Proyecto de un Android TOTALMENTE libre, muy interesante, sobretodo por que viendo los dispositivos que soporta se puede ver lo mal que está la industria. (https://www.replicant.us/)

F-droid (https://f-droid.org/) gestor de programas de software libre para móviles android, →alternativa a google play.

GTA04 y proyectos que ponen la libertad en el centro: (http://openphoenux.org/) (https://shop.goldelico.com/wiki.php?page=GTA04) → GTA04 es el teléfono móvil prioritario para la comunidad Replicant.

#### Proyectos comerciales:

Purism (https://puri.sm) → se denominan como corporación de propósito social, desarrolla teléfonos con un sistema operativo propio basado en GNU/Linux. (hay críticas y no han conseguido certificación por parte de la FSF, pero ahí está)

Fairphone (https://www.fairphone.com/en/) teléfono móvil ético respecto a temas de trazabilidad de los materiales empleados.

Lineage os (https://www.lineageos.org/) versión de android un poco más “ética” viene de un proyecto comunitario llamado cyanogen mod.

Cyanogen Mod (https://www.cyanogenmods.org/) antiguo nombre de las versiones de lineage os. Es interesante porque portan su sistema a otros móviles, o lo instalan en dispositivos de forma que garantizan que se puede personalziar y cambiar el sistema operativo.

#### Proyectos de háztelo tú mismo:

Zerophone (https://wiki.zerophone.org/)

y piPhone (http://www.davidhunt.ie/piphone-a-raspberry-pi-based-smartphone/)

Proyectos curiosos de teléfonos montados a mano con SBC raspberry pi. No llegan a ser smartphones, el raspberry pi no es totalmente libre, pero es interesante como alternativa si más proyectos se suman y avanza, pues es modular y autoconstruible.
